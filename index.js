// main index.js

import { NativeModules } from 'react-native';

const { ReactNativeCryptoStandalone } = NativeModules;

export default ReactNativeCryptoStandalone;
